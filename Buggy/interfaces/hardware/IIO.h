#pragma once

#include <Arduino.h>

/**
 * @brief Describes a generic input output device.
 * 
 */
class IIO
{
	public:
		IIO() { return; }
		virtual ~IIO() { return; }

		virtual uint16_t GetAvailableForRead() = 0;

		/**
		 * @brief Read a single byte from the device.
		 * 
		 * @return uint8_t The byte read.
		 */
		virtual uint8_t ReadByte() = 0;
		/**
		 * @brief Read n number of bytes from the device.
		 * 
		 * @param uiLen Number of bytes to read.
		 * @param ubypBytes Pointer to the buffer to record the bytes to.
		 * @return uint16_t Number of bytes read and stored in the buffer
		 */
		virtual uint16_t ReadMany(uint16_t uiLen, uint8_t * ubypBytes) = 0;
		/**
		 * @brief Write a byte to the device
		 * 
		 * @param ubyByte Value to write. 
		 * @return true Successfully written.
		 * @return false Failed to write.
		 */
		virtual void Write(uint8_t ubyByte) = 0;
		/**
		 * @brief Write multiple bytes to the device.
		 * 
		 * @param uiLen Number of bytes to write.
		 * @param ubypBytes Buffer containing the bytes to write.
		 * @return true Successfully written.
		 * @return false Failed to write.
		 */
		virtual void WriteMany(uint16_t uiLen, uint8_t* ubypBytes) = 0;
};