#pragma once
#ifndef BUGGY_I_MOTOR_H
#define BUGGy_I_MOTOR_H

class IStepperMotor
{
	public:
		enum class Direction : uint8_t
		{
			CounterClockwise = 0,
			Clockwise = 1
		};

		virtual void Start() = 0;
		virtual void Stop() = 0;
		virtual void Drive() = 0;
		//returns the step frequency based on a percentage passed in that denotes the speed between 0% slowest and 100% maximum, to stop the buggy call stop
		virtual uint16_t SetSpeed(float fSpeedPercentage) = 0;
		virtual void SetDirection(Direction dir) = 0;
		virtual Direction GetDirection() const = 0;
		virtual bool IsRunning() const = 0;
		virtual uint16_t GetMinFrequency() const = 0;
		virtual uint16_t GetMaxFrequency() const = 0;
		virtual void ClockwiseIsCCW(bool b) = 0;
};

#endif