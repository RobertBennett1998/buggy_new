#pragma once

class IInterruptEventListener
{
    friend class InterruptManager;  
    protected:
		virtual void TimerOneTriggered() 
		{
			return;
		}

        virtual void TimerThreeTriggered() 
		{
			return;
		}

        virtual void TimerFourTriggered() 
		{
			return;
		}
};