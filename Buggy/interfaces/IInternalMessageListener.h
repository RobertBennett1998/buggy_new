#pragma once
#ifndef BUGGY_I_INTERNAL_MESSAGE_H
#define BUGGY_I_INTERNAL_MESSAGE_H

#include "../headers/PacketDefs.h"

class IInternalMessageListener
{
	public:
		virtual void InternalMessageRecieved(InternalMessageIds id, void* pMsg) = 0;
		virtual void BluetoothMessageRecieved(IncomingMessageIds id, void* pMsg) = 0;
};

#endif