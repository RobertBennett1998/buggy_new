#include "..\..\headers\HardwareManagers\MotorDriver.h"

MotorDriver::MotorDriver(IStepperMotor* pLeftMotor, IStepperMotor* pRightMotor, CommunicationManager* pComsManager) :
m_pLeftStepperMotor(pLeftMotor),
m_pRightStepperMotor(pRightMotor),
m_pCommunicationManager(pComsManager)
{
	m_pCommunicationManager->RegisterInternalMessageListener(this);
}

MotorDriver::~MotorDriver()
{
	m_pCommunicationManager->UnregisterInternalMessageListener(this);
}

void MotorDriver::Intialize()
{
	InterruptManager::GetInstance()->CreateTimerThree(0, 0x03, 0x01, m_pLeftStepperMotor->GetMinFrequency());
	InterruptManager::GetInstance()->CreateTimerFour(0, 0x03, 0x01, m_pRightStepperMotor->GetMinFrequency());
	InterruptManager::GetInstance()->SetTimerThreeListener(this);
	InterruptManager::GetInstance()->SetTimerFourListener(this);

	m_pLeftStepperMotor->ClockwiseIsCCW(true);
}

void MotorDriver::TimerThreeTriggered()
{
	m_pLeftStepperMotor->Drive();
}

void MotorDriver::TimerFourTriggered()
{
	m_pRightStepperMotor->Drive();
}

void MotorDriver::BluetoothMessageRecieved(IncomingMessageIds id, void * pMsg)
{
	switch (id)
	{
		default:
			break;

		case IncomingMessageIds::SetMotorParams:
			//Serial.println((uint8_t)id);

			IncomingMessages::SetMotorParams* pMotorParams = static_cast<IncomingMessages::SetMotorParams*>(pMsg);
			if (pMotorParams == nullptr)
			{
				Serial.println("nullptr");
				return;
			}

			m_pLeftStepperMotor->SetDirection((IStepperMotor::Direction)pMotorParams->ubyLeftDirection);
			InterruptManager::GetInstance()->UpdateTimerThreeCount(m_pLeftStepperMotor->SetSpeed(pMotorParams->ubyLeftSpeedPercentage));
			m_pRightStepperMotor->SetDirection((IStepperMotor::Direction)pMotorParams->ubyRightDirection);
			InterruptManager::GetInstance()->UpdateTimerFourCount(m_pRightStepperMotor->SetSpeed(pMotorParams->ubyRightSpeedPercentage));

			if (pMotorParams->ubyLeftSpeedPercentage > 0)
			{
				m_pLeftStepperMotor->Start();
			}
			else
			{
				m_pLeftStepperMotor->Stop();
			}

			if (pMotorParams->ubyRightSpeedPercentage > 0)
			{
				m_pRightStepperMotor->Start();
			}
			else
			{
				m_pRightStepperMotor->Stop();
			}

			break;
	}
}

void MotorDriver::InternalMessageRecieved(InternalMessageIds id, void * pMsg)
{
	switch (id)
	{
		default:
			break;

		case InternalMessageIds::ConnectionLost:
			m_pLeftStepperMotor->Stop();
			m_pRightStepperMotor->Stop();
			break;
	}
}
