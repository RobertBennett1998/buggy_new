#include "..\headers\BuggyManager.h"

BuggyManager::BuggyManager() :
m_bUpdate(false),
m_pBluetoothModule(nullptr),
m_pCommunicationManager(nullptr),
m_pLeftMotor(nullptr),
m_pRightMotor(nullptr),
m_pMotorDriver(nullptr)
{
	return;
}

BuggyManager::~BuggyManager()
{
	Shutdown();
	return;
}

void BuggyManager::Initialize()
{
	m_pBluetoothModule = new HC06Bluetooth(&Serial3, 9600);

	m_pCommunicationManager = new CommunicationManager(m_pBluetoothModule);

	/**
	* Use the calculate_timer python script to calculate the initial timer value
	*
	* Prescaler hex value:
	* 		 8 - 0x02
	* 		64 - 0x03
	*     256 - 0x04
	*    1024 - 0x05
	*
	* Current timer value: 100ms
	*
	*/
	InterruptManager::GetInstance()->CreateTimerOne(0x00, 0x03, 0x01, 0x9e58);
	InterruptManager::GetInstance()->SetTimerOneListener(this);

	m_pLeftMotor = new Nema17(6, 7, 8, 10, 11, 9, 0.0663);
	m_pRightMotor = new Nema17(43, 41, 39, 5, 4, 37, 0.0663); 

	m_pMotorDriver = new MotorDriver(m_pLeftMotor, m_pRightMotor, m_pCommunicationManager);

	m_pMotorDriver->Intialize();

	return;
}

void BuggyManager::Update()
{
	//As fast as possible
	m_pCommunicationManager->Update();
	//every 100 ms
	if (m_bUpdate)
	{
		static uint8_t ubyCount = 0;

		//every 300ms
		if (ubyCount >= 3)
		{
			m_pCommunicationManager->CheckConnectionStatus();
			ubyCount = 0;
		}

		
		ubyCount++;
		m_bUpdate = false;
	}
}

void BuggyManager::Shutdown()
{
	if (m_pMotorDriver != nullptr)
	{
		delete m_pMotorDriver;
		m_pMotorDriver = nullptr;
	}

	if (m_pLeftMotor != nullptr)
	{
		delete m_pLeftMotor;
		m_pLeftMotor = nullptr;
	}

	if (m_pRightMotor != nullptr)
	{
		delete m_pRightMotor;
		m_pRightMotor = nullptr;
	}

	if (m_pCommunicationManager != nullptr)
	{
		delete m_pCommunicationManager;
		m_pCommunicationManager = nullptr;
	}

	if (m_pBluetoothModule != nullptr)
	{
		delete m_pBluetoothModule;
		m_pBluetoothModule = nullptr;
	}
}

void BuggyManager::TimerOneTriggered()
{
	m_bUpdate = true;
}