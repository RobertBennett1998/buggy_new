#include "../../headers/Hardware/Nema17.h"

Nema17::Nema17(uint8_t ubyMs1, uint8_t ubyMs2, uint8_t ubyMs3, uint8_t ubyDir, uint8_t ubyStep, uint8_t ubyReset, float fWheelDiameterInMeters) :
m_bIsRunning(false),
m_Direction(Direction::Clockwise),
m_Gear(Gears::First),
m_ubyMs1Pin(ubyMs1),
m_ubyMs2Pin(ubyMs2),
m_ubyMs3Pin(ubyMs3),
m_ubyDirPin(ubyDir),
m_ubyStepPin(ubyStep),
m_ubyResetPin(ubyReset),
m_fWheelDiameterInMeters(fWheelDiameterInMeters),
m_bClockwiseIsCCW(false)
{
	pinMode(m_ubyMs1Pin, OUTPUT);
	pinMode(m_ubyMs2Pin, OUTPUT);
	pinMode(m_ubyMs3Pin, OUTPUT);
	pinMode(m_ubyDirPin, OUTPUT);
	pinMode(m_ubyStepPin, OUTPUT);
	pinMode(m_ubyResetPin, OUTPUT);

	digitalWrite(m_ubyMs1Pin, LOW);
	digitalWrite(m_ubyMs2Pin, LOW);
	digitalWrite(m_ubyMs3Pin, LOW);
	digitalWrite(m_ubyDirPin, LOW);
	digitalWrite(m_ubyStepPin, LOW);
	digitalWrite(m_ubyResetPin, LOW);

	SetGear(m_Gear);
}

Nema17::~Nema17()
{
}


void Nema17::Start()
{
	digitalWrite(m_ubyResetPin, HIGH);
	m_bIsRunning = true;
}

void Nema17::Stop()
{
	digitalWrite(m_ubyResetPin, LOW);
	m_bIsRunning = false;
}

void Nema17::Drive()
{
	if (m_bIsRunning)
	{
		//time taken for write to occur twice is long enough for a "step"
		digitalWrite(m_ubyStepPin, HIGH);
		digitalWrite(m_ubyStepPin, LOW);
	}
}

uint16_t Nema17::SetSpeed(float fSpeedPercentage)
{
	//Serial.println(fSpeedPercentage);
	float fLinearVelocity = (fSpeedPercentage / 100.0f) * 0.3f;
	static float fWheelDiameter = 0.0663f;
	static float fWheelRadius = fWheelDiameter / 2.0f;
	static float fWheelCircumfrence = fWheelDiameter * PI;
	//static float fStepAngle = (PI / 180.0f) * 1.8f;

	float fAngularSpeed = fLinearVelocity / fWheelRadius;

	//Serial.println(fAngularSpeed);

	if (fAngularSpeed <= 0.24f)
	{
		SetGear(Gears::First);
	}
	else if (fAngularSpeed <= 0.8f)
	{
		SetGear(Gears::Second);
	}
	else if (fAngularSpeed <= 1.5f)
	{
		SetGear(Gears::Third);
	}
	else if (fAngularSpeed <= 3.0f)
	{
		SetGear(Gears::Fourth);
	}
	else
	{
		SetGear(Gears::Fifth);
	}

	float fRPS = fAngularSpeed / (2.0f * PI);
	float fStepsPerSecond = fRPS * (uint16_t)m_Gear;

	float fTimePerStep = 1.0f / fStepsPerSecond;
	float fFreq = fTimePerStep * 16000000;

	uint16_t uiTimerStart = 0x0000 - (uint16_t)(fFreq / 64);

	//Serial.println(uiTimerStart);

	return uiTimerStart;
}

void Nema17::SetDirection(Direction dir)
{
	m_Direction = dir;

	if (m_Direction == Direction::Clockwise)
	{
		if (m_bClockwiseIsCCW)
		{
			digitalWrite(m_ubyDirPin, HIGH);
		}
		else
		{
			digitalWrite(m_ubyDirPin, LOW);
		}
	}
	else if (m_Direction == Direction::CounterClockwise)
	{
		if (m_bClockwiseIsCCW)
		{
			digitalWrite(m_ubyDirPin, LOW);
		}
		else
		{
			digitalWrite(m_ubyDirPin, HIGH);
		}
	}
}

IStepperMotor::Direction Nema17::GetDirection() const
{
	return m_Direction;
}

bool Nema17::IsRunning() const
{
	return m_bIsRunning;
}

uint16_t Nema17::GetMinFrequency() const
{
	return m_uiMinSpeedCount;
}

uint16_t Nema17::GetMaxFrequency() const
{
	return m_uiMaxSpeedCount;
}

void Nema17::SetGear(Gears gear)
{
	m_Gear = gear;
	switch (gear)
	{
	default:
		//Serial.println("0");
		break;

	case Gears::Fifth:
		//Serial.println("5");
		digitalWrite(m_ubyMs1Pin, LOW);
		digitalWrite(m_ubyMs2Pin, LOW);
		digitalWrite(m_ubyMs3Pin, LOW);
		break;

	case Gears::Fourth:
		//Serial.println("4");
		digitalWrite(m_ubyMs1Pin, HIGH);
		digitalWrite(m_ubyMs2Pin, LOW);
		digitalWrite(m_ubyMs3Pin, LOW);
		break;

	case Gears::Third:
		//Serial.println("3");
		digitalWrite(m_ubyMs1Pin, LOW);
		digitalWrite(m_ubyMs2Pin, HIGH);
		digitalWrite(m_ubyMs3Pin, LOW);
		break;

	case Gears::Second:
		//Serial.println("2");
		digitalWrite(m_ubyMs1Pin, HIGH);
		digitalWrite(m_ubyMs2Pin, HIGH);
		digitalWrite(m_ubyMs3Pin, LOW);
		break;

	case Gears::First:
		//Serial.println("1");
		digitalWrite(m_ubyMs1Pin, HIGH);
		digitalWrite(m_ubyMs2Pin, HIGH);
		digitalWrite(m_ubyMs3Pin, HIGH);
		break;
	}
}

void Nema17::ClockwiseIsCCW(bool b)
{
	m_bClockwiseIsCCW = b;
}