#include "../../headers/Hardware/HC06Bluetooth.h"

HC06Bluetooth::HC06Bluetooth(HardwareSerial * const SerialPort, uint32_t ulBaudRate) :
m_pSerial(SerialPort),
m_ulBaudRate(ulBaudRate)
{
	m_pSerial->begin(m_ulBaudRate);
}

HC06Bluetooth::~HC06Bluetooth()
{
}

uint16_t HC06Bluetooth::GetAvailableForRead()
{
	return m_pSerial->available();
}

uint8_t HC06Bluetooth::ReadByte()
{
	if (m_pSerial->available() > 0)
		return m_pSerial->read();

	return 0;
}

uint16_t HC06Bluetooth::ReadMany(uint16_t uiLen, uint8_t * ubypBytes)
{
	if (uiLen == 0)
		return 0;

	if (uiLen > m_pSerial->available())
		return 0;

	m_pSerial->readBytes(ubypBytes, uiLen);
	return uiLen;
}

void HC06Bluetooth::Write(uint8_t ubyByte)
{
	if (m_pSerial->availableForWrite() != 0)
		m_pSerial->write(ubyByte);
}

void HC06Bluetooth::WriteMany(uint16_t uiLen, uint8_t * ubypBytes)
{
	if (uiLen == 0)
		return;

	if (uiLen > m_pSerial->availableForWrite())
		return;

	m_pSerial->write(ubypBytes, uiLen);
}
