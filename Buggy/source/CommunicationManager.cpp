#include "../headers/CommunicationManager.h"

CommunicationManager::CommunicationManager(IIO* pIODevice) :
m_pIODevice(pIODevice),
m_bRepliedToLastCheckConnectionPacket(false),
m_ubyMissedCheckConnectionPackets(3)
{
	pinMode(3, OUTPUT);
	digitalWrite(3, LOW);
	return;
}

CommunicationManager::~CommunicationManager()
{
	return;
}

void CommunicationManager::Update()
{
	CheckForPacket();
}

void CommunicationManager::CheckConnectionStatus()
{
	if (m_bRepliedToLastCheckConnectionPacket)
	{
		m_ubyMissedCheckConnectionPackets = 0;
	}
	else
	{
		if(m_ubyMissedCheckConnectionPackets < 0xFF)
			m_ubyMissedCheckConnectionPackets++;
	}

	if (m_ubyMissedCheckConnectionPackets >= 3)
	{
		RaiseInternalMessage(InternalMessageIds::ConnectionLost);
		digitalWrite(3, LOW);
	}
	else
	{
		digitalWrite(3, HIGH);
	}

	m_bRepliedToLastCheckConnectionPacket = false;

	OutgoingMessages::CheckConnection cc;
	cc.ubyIdentity = 0xFE;
	SendMessage(OutgoingMessageIds::CheckConnection, &cc);
}

void CommunicationManager::RegisterInternalMessageListener(IInternalMessageListener * pLst)
{
	for (uint16_t i = 0; i < m_InternalMessageListeners.Count(); i++)
	{
		if (m_InternalMessageListeners[i] == pLst)
		{
			return;
		}
	}

	m_InternalMessageListeners.Add(pLst);
}

void CommunicationManager::UnregisterInternalMessageListener(IInternalMessageListener * pLst)
{
	for (uint16_t i = 0; i < m_InternalMessageListeners.Count(); i++)
	{
		if (m_InternalMessageListeners[i] == pLst)
		{
			m_InternalMessageListeners.RemoveAt(i);
		}
	}
}

void CommunicationManager::RaiseInternalMessage(InternalMessageIds id, void * pMsg)
{
	for (uint16_t i = 0; i < m_InternalMessageListeners.Count(); i++)
		m_InternalMessageListeners[i]->InternalMessageRecieved(id, pMsg);
}

void CommunicationManager::RaiseInternalBluetoothMessage(IncomingMessageIds id, void * pMsg)
{
	for (uint16_t i = 0; i < m_InternalMessageListeners.Count(); i++)
		m_InternalMessageListeners[i]->BluetoothMessageRecieved(id, pMsg);
}

inline void CommunicationManager::CheckForPacket()
{
	static bool bReadingMessage = false;
	static uint8_t ubyMsgLen = 0;
	static uint8_t* pHeader = new uint8_t[4];

	int availableBytes = m_pIODevice->GetAvailableForRead();
	if (!availableBytes) return;

	if (!bReadingMessage && availableBytes >= sizeof(Header))
	{
		pHeader[0] = m_pIODevice->ReadByte();
		if (pHeader[0] == (uint8_t)0xBA)
		{
			pHeader[1] = m_pIODevice->ReadByte();
			if (pHeader[1] == (uint8_t)0xE0)
			{ 
				m_pIODevice->ReadMany(sizeof(Header) - 2, pHeader + 2);
				ubyMsgLen = pHeader[3];
				bReadingMessage = !bReadingMessage;
			}
		}
	}
	// Refresh the number of available bytes in case some have been sent during header processing
	availableBytes = m_pIODevice->GetAvailableForRead();
	if (bReadingMessage && availableBytes >= ubyMsgLen + sizeof(Footer))
	{
		uint8_t ubyPckLen = sizeof(Header) + ubyMsgLen + sizeof(Footer);
		uint8_t *pPck = new uint8_t[ubyPckLen];
		memcpy(pPck, pHeader, sizeof(Header));
		m_pIODevice->ReadMany(ubyMsgLen + sizeof(Footer), pPck + sizeof(Header));
		if (pPck[ubyPckLen - 2] != (uint8_t)0x0E || pPck[ubyPckLen - 1] != (uint8_t)0xAB)
		{
			Serial.print("Footer invalid (pck len/ident): ");
			Serial.print(ubyPckLen);
			Serial.print(", ");
			Serial.print(pPck[ubyPckLen - 2]);
			Serial.print(", ");
			Serial.println(pPck[ubyPckLen - 1]);
		}
		else
		{
			ProcessBluetoothPacket(pPck, ubyPckLen);
		}
		ubyMsgLen = 0;
		bReadingMessage = !bReadingMessage;
		delete[] pPck;
	}
}

inline void CommunicationManager::ProcessBluetoothPacket(uint8_t * pPck, uint8_t ubyPckLen)
{
	Header header = ExtractHeader(pPck, ubyPckLen);
	//Serial.println(header.ubyMessageID);
	switch ((IncomingMessageIds)header.ubyMessageID)
	{
		default:
			break;
		case IncomingMessageIds::CheckConnection:
			if (pPck[sizeof(Header)] == 0xFE)
			{
				m_bRepliedToLastCheckConnectionPacket = true;
			}
			break;

	}	

	RaiseInternalBluetoothMessage((IncomingMessageIds)header.ubyMessageID, pPck + sizeof(Header));
}

inline Header CommunicationManager::ExtractHeader(uint8_t * pPck, uint8_t ubyPckLen)
{
	if (sizeof(Header) + sizeof(Footer) > ubyPckLen)
		return;

	Header ret;
	memcpy(&ret, pPck, sizeof(Header));

	return ret;
}
