#include "../headers/InterruptManager.h"

////////
//HACK//
////////
//this is probably hacky but it works
//The definition is hidden so it qualifies as an incomplete type which will stop anyone from calling these methods
class CInterruptManagerHidden
{
	public:
		static void Timer1Relay()
		{
			InterruptManager::GetInstance()->TimerOneTrigger();
		}

		static void Timer3Relay()
		{
			InterruptManager::GetInstance()->TimerThreeTrigger();
		}

		static void Timer4Relay()
		{
			InterruptManager::GetInstance()->TimerFourTrigger();
		}
};

InterruptManager* InterruptManager::sm_pInstance = nullptr;

ISR(TIMER1_OVF_vect)
{
	CInterruptManagerHidden::Timer1Relay();
}

ISR(TIMER3_OVF_vect)
{
	CInterruptManagerHidden::Timer3Relay();
}

ISR(TIMER4_OVF_vect)
{
	CInterruptManagerHidden::Timer4Relay();
}

InterruptManager::InterruptManager() :
m_ubyTimerOneRegA(0),
m_ubyTimerOneRegB(0),
m_ubyTimerOneMask(0),
m_uiTimerOneCount(0),
m_ubyTimerThreeRegA(0),
m_ubyTimerThreeRegB(0),
m_ubyTimerThreeMask(0),
m_uiTimerThreeCount(0),
m_ubyTimerFourRegA(0),
m_ubyTimerFourRegB(0),
m_ubyTimerFourMask(0),
m_uiTimerFourCount(0),
m_pTimerOneListener(nullptr),
m_pTimerThreeListener(nullptr),
m_pTimerFourListener(nullptr)
{
	DisableInterrupts();

	return;
}

InterruptManager::~InterruptManager()
{
	DisableInterrupts();
	return;
}

void InterruptManager::CreateTimerOne(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount)
{
	m_ubyTimerOneRegA = ubyRegA;
	m_ubyTimerOneRegB = ubyRegB;
	m_ubyTimerOneMask = ubyMask;
	m_uiTimerOneCount = uiCount;

	TCCR1A = m_ubyTimerOneRegA;   
	TCCR1B = m_ubyTimerOneRegB;
	TCNT1 = m_uiTimerOneCount;
	TIMSK1 = m_ubyTimerOneMask; 
}

void InterruptManager::UpdateTimerOneCount(uint16_t uiCount)
{
	m_uiTimerOneCount = uiCount;
}

void InterruptManager::SetTimerOneListener(IInterruptEventListener * pListener)
{
	if (m_pTimerOneListener == nullptr)
		m_pTimerOneListener = pListener;
}

void InterruptManager::CreateTimerThree(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount)
{
	m_ubyTimerThreeRegA = ubyRegA;
	m_ubyTimerThreeRegB = ubyRegB;
	m_ubyTimerThreeMask = ubyMask;
	m_uiTimerThreeCount = uiCount;

	TCCR3A = m_ubyTimerThreeRegA;    // set entire TCCR3A register to 0
	TCCR3B = m_ubyTimerThreeRegB;// (1 << CS10) | (1 << CS11); //set prescaler to 64
	TCNT3 = m_uiTimerThreeCount; //overflow every 20 milli seconds
	TIMSK3 = m_ubyTimerThreeMask; //timer3 overflow interrupt enable
}

void InterruptManager::UpdateTimerThreeCount(uint16_t uiCount)
{
	m_uiTimerThreeCount = uiCount;
}

void InterruptManager::SetTimerThreeListener(IInterruptEventListener * pListener)
{
	if (m_pTimerThreeListener == nullptr)
		m_pTimerThreeListener = pListener;
}

void InterruptManager::CreateTimerFour(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount)
{
	m_ubyTimerFourRegA = ubyRegA;
	m_ubyTimerFourRegB = ubyRegB;
	m_ubyTimerFourMask = ubyMask;
	m_uiTimerFourCount = uiCount;

	TCCR4A = m_ubyTimerFourRegA;    // set entire TCCR3A register to 0
	TCCR4B = m_ubyTimerFourRegB;// (1 << CS10) | (1 << CS11); //set prescaler to 64
	TCNT4 = m_uiTimerFourCount; //overflow every 20 milli seconds
	TIMSK4 = m_ubyTimerFourMask; //timer3 overflow interrupt enab
}

void InterruptManager::UpdateTimerFourCount(uint16_t uiCount)
{
	m_uiTimerFourCount = uiCount;

}

void InterruptManager::SetTimerFourListener(IInterruptEventListener * pListener)
{
	if (m_pTimerFourListener == nullptr)
		m_pTimerFourListener = pListener;
}

void InterruptManager::DisableInterrupts()
{
	cli();
}

void InterruptManager::EnableInterrupts()
{
	sei();
}

void InterruptManager::TimerOneTrigger()
{
	if (m_pTimerOneListener != nullptr)
		m_pTimerOneListener->TimerOneTriggered();

	TCNT1 = m_uiTimerOneCount;
}

void InterruptManager::TimerThreeTrigger()
{
	if (m_pTimerThreeListener != nullptr)
		m_pTimerThreeListener->TimerThreeTriggered();

	TCNT3 = m_uiTimerThreeCount;
}

void InterruptManager::TimerFourTrigger()
{
	if (m_pTimerFourListener != nullptr)
		m_pTimerFourListener->TimerFourTriggered();

	TCNT4 = m_uiTimerFourCount;
}