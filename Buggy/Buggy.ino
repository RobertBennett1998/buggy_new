#include <Arduino.h>

#include "headers/BuggyManager.h"
#include "headers/InterruptManager.h"

BuggyManager g_BuggyManager;

void setup() 
{
	Serial.begin(9600);
	g_BuggyManager.Initialize();
	InterruptManager::GetInstance()->EnableInterrupts();
}

void loop()
{
	g_BuggyManager.Update();
}