#pragma once
#ifndef BUGGY_NEMA_17_H
#define BUGGY_NEMA_17_H

#include <Arduino.h>

#include "../../interfaces/hardware/IStepperMotor.h"

class Nema17 : public IStepperMotor
{
	private:
		enum class Gears : uint16_t
		{
			First = 3200,
			Second = 1600,
			Third = 800,
			Fourth = 400,
			Fifth = 200
		};

	public:
		Nema17(uint8_t ubyMs1, uint8_t ubyMs2, uint8_t ubyMs3, uint8_t ubyDir, uint8_t ubyStep, uint8_t ubyReset, float fWheelDiameterInMeters);
		~Nema17();

		// Inherited via IStepperMotor
		virtual void Start();
		virtual void Stop();
		virtual void Drive();
		virtual uint16_t SetSpeed(float fSpeedPercentage);
		virtual void SetDirection(Direction dir);
		virtual Direction GetDirection() const;
		virtual bool IsRunning() const;
		virtual uint16_t GetMinFrequency() const;
		virtual uint16_t GetMaxFrequency() const;
		virtual void ClockwiseIsCCW(bool b);

	private:
		bool m_bIsRunning, m_bClockwiseIsCCW;
		Direction m_Direction;
		Gears m_Gear;
		uint8_t m_ubyMs1Pin, m_ubyMs2Pin, m_ubyMs3Pin, m_ubyDirPin, m_ubyStepPin, m_ubyResetPin;
		float m_fWheelDiameterInMeters;
		const float m_fMaxLinearSpeedInMetersPerSecond = 0.2;

		void SetGear(Gears gear);

		//extra speed 0xFD50
		const uint16_t m_uiMaxSpeedCount = 0xFC00;
		const uint16_t m_uiMinSpeedCount = 0xE800;
};

#endif