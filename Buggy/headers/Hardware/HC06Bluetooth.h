#pragma once

#include "../../interfaces/hardware/IIO.h"

#include <Arduino.h>

class HC06Bluetooth : public IIO
{
	public:
		/**
		* @brief Construct a new HC06Bluetooth object
		* 
		* @param SerialPort The serial line the BT hardware component uses.
		* @param ulBaudRate The baud rate of the device
		*/
		HC06Bluetooth(HardwareSerial * const SerialPort, uint32_t ulBaudRate);
		~HC06Bluetooth();

		// Inherited via IIO
		virtual uint16_t GetAvailableForRead() override;
		virtual uint8_t ReadByte() override;
		virtual uint16_t ReadMany(uint16_t uiLen, uint8_t * ubypBytes) override;
		virtual void Write(uint8_t ubyByte) override;
		virtual void WriteMany(uint16_t uiLen, uint8_t * ubypBytes) override;

	private:
		HC06Bluetooth();

		HardwareSerial * const m_pSerial;
		uint32_t m_ulBaudRate;
};