#pragma once
#ifndef BUGGY_DRIVER_H
#define BUGGY_DRIVER_H

#include "../../headers/InterruptManager.h"
#include "../../interfaces/IInterruptEventListener.h"
#include "../../interfaces/hardware/IStepperMotor.h"
#include "../../interfaces/IInternalMessageListener.h"
#include "../../headers/CommunicationManager.h"

class MotorDriver : public IInterruptEventListener, public IInternalMessageListener
{
	public:
		MotorDriver(IStepperMotor* pLeftMotor, IStepperMotor* pRightMotor, CommunicationManager* pComsManager);
		//Possible to overload e.g
			//MotorDriver(IDCMotor* pLeftMotor, IDCMotor* pRightMotor);
		~MotorDriver();

		void Intialize();

	protected:
		virtual void TimerThreeTriggered();
		virtual void TimerFourTriggered();

	private:

		IStepperMotor* m_pLeftStepperMotor;
		IStepperMotor* m_pRightStepperMotor;

		// Inherited via IInternalMessageListener
		virtual void InternalMessageRecieved(InternalMessageIds id, void * pMsg);
		virtual void BluetoothMessageRecieved(IncomingMessageIds id, void * pMsg);

		CommunicationManager* m_pCommunicationManager;
};

#endif