#pragma once
#ifndef BUGGY_PACKET_DEFS_H
#define BUGGY_PACKET_DEFS_H

#include <Arduino.h>

struct Header
{
	public:
		uint16_t uiIdentity;
		uint8_t ubyMessageID;
		uint8_t ubyMessageLength;
};

struct Footer
{
	public:
		uint16_t uiIdentity;
};

//1...127
enum class IncomingMessageIds : uint8_t
{
	Undefined = 0,
	CheckConnection = 1,
	SetMotorParams = 2,
};

namespace IncomingMessages
{
	struct CheckConnection
	{
		public:
			uint8_t ubyIdentity;
	};

	struct SetMotorParams
	{
		public:
			uint8_t ubyLeftSpeedPercentage;
			uint8_t ubyLeftDirection;
			uint8_t ubyRightSpeedPercentage;
			uint8_t ubyRightDirection;
	};
}

//128...255 with the exception of check connection
enum class OutgoingMessageIds : uint8_t
{
	Undefined = 0,
	CheckConnection = 1
};

namespace OutgoingMessages
{
	struct CheckConnection
	{
		public:
			uint8_t ubyIdentity;
	};
}

enum class InternalMessageIds : uint8_t
{
	Undefined = 0,
	ConnectionLost = 1
};

#endif