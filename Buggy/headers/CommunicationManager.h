#pragma once
#ifndef BUGGY_COMMUNICATION_MANAGER_H
#define BUGGY_COMMUNICATION_MANAGER_H

#include "../interfaces/IInternalMessageListener.h"
#include "../utility/List.hpp"
#include "../headers/PacketDefs.h"
#include "../interfaces/hardware/IIO.h"

class CommunicationManager
{
	friend class Bluetooth;
	public:
		CommunicationManager(IIO* pIODevice);
		~CommunicationManager();

		void Update();
		void CheckConnectionStatus();

		template<class T>
		void SendMessage(OutgoingMessageIds id, T* pMsg);

		void RegisterInternalMessageListener(IInternalMessageListener* pLst);
		void UnregisterInternalMessageListener(IInternalMessageListener* pLst);

	protected:
		void RaiseInternalMessage(InternalMessageIds id, void* pMsg = nullptr);
		void RaiseInternalBluetoothMessage(IncomingMessageIds id, void* pMsg = nullptr);

	private:
		CommunicationManager();
		IIO * m_pIODevice;
		List<IInternalMessageListener*> m_InternalMessageListeners;
		bool m_bRepliedToLastCheckConnectionPacket;
		uint8_t m_ubyMissedCheckConnectionPackets;

		inline void CheckForPacket();
		inline void ProcessBluetoothPacket(uint8_t* pPck, uint8_t ubyPckLen);
		inline Header ExtractHeader(uint8_t* pPck, uint8_t ubyPckLen);
};

#endif

template<class T>
inline void CommunicationManager::SendMessage(OutgoingMessageIds id, T * pMsg)
{
	Header head;
	head.uiIdentity = 0xE0BA;
	head.ubyMessageID = (uint8_t)id;
	head.ubyMessageLength = sizeof(T);

	Footer foot;
	foot.uiIdentity = 0xAB0E;

	uint8_t* pPck = (uint8_t*)malloc(sizeof(Header) + sizeof(T) + sizeof(Footer));

	memcpy(pPck, &head, sizeof(Header));
	memcpy(pPck + sizeof(Header), pMsg, sizeof(T));
	memcpy(pPck + sizeof(Header) + sizeof(T), &foot, sizeof(Footer));

	m_pIODevice->WriteMany(sizeof(Header) + sizeof(T) + sizeof(Footer), pPck);

	free(pPck);
}