#pragma once
#ifndef BUGGY_INTERRUPT_MANAGER_H
#define BUGGY_INTERRUPT_MANAGER_H

#include <Arduino.h>
#include <avr/interrupt.h>

#include "../interfaces/IInterruptEventListener.h"

class InterruptManager
{
	private:
		//this little hack allows me to hide the timer triggers as the friend class is a wrapper in c++
		friend class CInterruptManagerHidden;

	public:
		static InterruptManager * GetInstance()
		{
			if (sm_pInstance == nullptr)
			{
				sm_pInstance = new InterruptManager();
			}

			return sm_pInstance;
		}

		/**
		 * @brief Create a timer in Timer 1
		 * 
		 * @param ubyRegA Control register A, most likely want to set this is 0x0000
		 * @param ubyRegB Control register B, containing clock prescaler controls
		 * @param ubyMask Timer interrupt mask register, set to 0x01 to enable interrupts
		 * @param uiCount Where to start counting, in order to overflow at the correct time
		 */
		void CreateTimerOne(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount);
		void UpdateTimerOneCount(uint16_t uiCount);
		void SetTimerOneListener(IInterruptEventListener* pListener);
		
		/**
		 * @brief Create a timer in Timer 3
		 * 
		 * @param ubyRegA Control register A, most likely want to set this is 0x0000
		 * @param ubyRegB Control register B, containing clock prescaler controls
		 * @param ubyMask Timer interrupt mask register, set to 0x01 to enable interrupts
		 * @param uiCount Where to start counting, in order to overflow at the correct time
		 */
		void CreateTimerThree(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount);
		void UpdateTimerThreeCount(uint16_t uiCount);
		void SetTimerThreeListener(IInterruptEventListener* pListener);
		
		/**
		 * @brief Create a timer in Timer 4
		 * 
		 * @param ubyRegA Control register A, most likely want to set this is 0x0000
		 * @param ubyRegB Control register B, containing clock prescaler controls
		 * @param ubyMask Timer interrupt mask register, set to 0x01 to enable interrupts
		 * @param uiCount Where to start counting, in order to overflow at the correct time
		 */
		void CreateTimerFour(uint8_t ubyRegA, uint8_t ubyRegB, uint8_t ubyMask, uint16_t uiCount);
		void UpdateTimerFourCount(uint16_t uiCount);
		void SetTimerFourListener(IInterruptEventListener* pListener);

		void DisableInterrupts();
		void EnableInterrupts();

	protected:
		void TimerOneTrigger();
		void TimerThreeTrigger();
		void TimerFourTrigger();

	private:
		InterruptManager();
		~InterruptManager();

		//Timer 1
		volatile uint8_t m_ubyTimerOneRegA;
		volatile uint8_t m_ubyTimerOneRegB;
		volatile uint8_t m_ubyTimerOneMask;
		volatile uint16_t m_uiTimerOneCount;

		//Timer3
		volatile uint8_t m_ubyTimerThreeRegA;
		volatile uint8_t m_ubyTimerThreeRegB;
		volatile uint8_t m_ubyTimerThreeMask;
		volatile uint16_t m_uiTimerThreeCount;

		//Timer 4
		volatile uint8_t m_ubyTimerFourRegA;
		volatile uint8_t m_ubyTimerFourRegB;
		volatile uint8_t m_ubyTimerFourMask;
		volatile uint16_t m_uiTimerFourCount;

		IInterruptEventListener* m_pTimerOneListener;
		IInterruptEventListener* m_pTimerThreeListener;
		IInterruptEventListener* m_pTimerFourListener;

		static InterruptManager* sm_pInstance;

};

#endif