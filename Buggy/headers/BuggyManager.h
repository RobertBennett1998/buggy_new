#pragma once
#ifndef BUGGY_BUGGY_MANAGER_H
#define BUGGY_BUGGY_MANAGER_H

#include "../headers/InterruptManager.h"
#include "../interfaces/IInterruptEventListener.h"
#include "../headers/HardwareManagers/MotorDriver.h"
#include "../headers/Hardware/Nema17.h"
#include "../headers/CommunicationManager.h"
#include "../headers/Hardware/HC06Bluetooth.h"

class BuggyManager : public IInterruptEventListener
{
	public:
		BuggyManager();
		~BuggyManager();

		void Initialize();
		void Update();
		void Shutdown();

	protected:
		virtual void TimerOneTriggered();

	private:
		bool m_bUpdate;
		HC06Bluetooth* m_pBluetoothModule;
		CommunicationManager* m_pCommunicationManager;
		Nema17* m_pLeftMotor, *m_pRightMotor;
		MotorDriver* m_pMotorDriver;
};

#endif