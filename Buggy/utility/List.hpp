#pragma once
#pragma once
#ifndef BUGGY_LIST_HPP
#define BUGGY_LIST_HPP

#include <Arduino.h>

//TODO: need to check for memory leaks for all data types.
//TODO: need to test if works for pointers to objects.

template <class T>
class List
{
	private:
		inline void ZeroMemory(void* ptr, uint16_t sz)
		{
			char* p = (char*)ptr;
			for (uint16_t i = 0; i < sz; i++)
				*(p + i) = 0;
		}

	public:
		List() :
			m_uiCapacity(0),
			m_uiCount(0),
			m_pData(nullptr)
		{
			return;
		}

		List(uint8_t uiCapacity) :
			m_uiCapacity(0),
			m_uiCount(0),
			m_pData(nullptr)
		{
			Reserve(uiCapacity);
			return;
		}

		~List()
		{
			delete[] m_pData;
		}

		inline T& operator[](uint16_t index)
		{
			if (index >= m_uiCount)
			{
				index = m_uiCount - 1;
			}

			return m_pData[index];
		}

		inline const T& operator[](uint16_t index) const
		{
			if (index >= m_uiCount)
			{
				index = m_uiCount - 1;
			}

			return m_pData[index];
		}

		void Add(T obj)
		{
			if (m_uiCount == m_uiCapacity)
				Reserve(m_uiCapacity + 1);

			*m_pNext = obj;
			m_pNext += 1;

			m_uiCount++;
		}

		/*void Add(T& obj)
		{
		if (m_uiCount == m_uiCapacity)
		Reserve(m_uiCapacity + 1);

		*m_pNext = obj;
		m_pNext += 1;

		m_uiCount++;
		}*/

		bool RemoveAt(uint16_t uiIdx)
		{
			if (m_uiCount == 0 || uiIdx > m_uiCount)
				return false;

			memcpy(&m_pData[uiIdx], &m_pData[uiIdx + 1], sizeof(T) * (m_uiCount - (uiIdx + 1)));

			ZeroMemory(&m_pData[m_uiCount - 1], sizeof(T));

			m_pNext = &m_pData[m_uiCount - 1];

			m_uiCount--;
			return true;
		}

		bool Reserve(uint16_t uiCapacity)
		{
			if (uiCapacity < m_uiCapacity && m_uiCount > uiCapacity)
				return false;

			T* pNewData = new T[uiCapacity];

			if (m_pData != nullptr)
			{
				memcpy(pNewData, m_pData, sizeof(T) * m_uiCount);
				if (m_uiCapacity == 1)
				{
					delete m_pData;
					m_pData = nullptr;
				}
				else
				{
					delete[] m_pData;
					m_pData = nullptr;
				}
			}

			m_pData = pNewData;

			m_uiCapacity = uiCapacity;
			m_pStart = &m_pData[0];
			m_pNext = &m_pData[m_uiCount];
			m_pEnd = &m_pData[m_uiCapacity - 1];

			return true;
		}

		const uint16_t Count() const
		{
			return m_uiCount;
		}

		const uint16_t Capacity() const
		{
			return m_uiCapacity;
		}

	private:
		uint16_t m_uiCount;
		uint16_t m_uiCapacity;

		T* m_pData;
		T* m_pStart;
		T* m_pNext;
		T* m_pEnd;
};

#endif